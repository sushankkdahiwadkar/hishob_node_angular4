import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { MessagesComponentComponent } from './messages-component/messages-component.component';
import { CreateMessageComponent } from './create-message/create-message.component';
import { NavComponentComponent } from './nav-component/nav-component.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
}
