import { Component, OnInit} from '@angular/core';
import { WebService } from '../messages-component/web.services';

@Component({
  selector: 'app-create-message',
  templateUrl: './create-message.component.html',
  styleUrls: ['./create-message.component.css']
})

export class CreateMessageComponent implements OnInit {

  constructor(private webService: WebService) { }

  bank = {
    bankName : ''
  };

  ngOnInit() {
  }

  post() {
    this.webService.postBank(this.bank);
  }
}
