import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Injectable } from '@angular/core';
import { MdSnackBar } from '@angular/material';

@Injectable()
export class WebService {
    constructor(private http: Http, private sb: MdSnackBar) {
        this.getMessage();
    }

    messages = [];

    async getMessage() {
        try {
            var response = await this.http.get('http://localhost:3000/bank').toPromise();
       this.messages = response.json();
        } catch (error) {
            this.handleerror("Unable to get bank list");
        }
    }

    async postBank(bank) {

        try {
            var response =  await this.http.post('http://localhost:3000/bank', bank).toPromise();
            this.messages.push(response.json());
        } catch (error) {
            this.handleerror("Unable to post bank");
        }
    }

    private handleerror(error) {
        console.error(error);
        this.sb.open(error, 'close', {duration : 2000});
    }
}
