var db = require('../config/dbconnection');

var bankAccount = {
    getBankAccounts : function (callback) {
        return db.query("SELECT * FROM bank_account", callback);
    },
    getBankAccountByBankAccountId : function (bankAccountId, callback) {
        return db.query("SELECT * FROM bank_account WHERE bank_account_id = ?", [bankAccountId], callback);
    },
    createBankAccount : function (bank, uuid, callback) {
        return db.query("INSERT INTO bank_account (uuid, account_number, account_type, user_id, bank_id, created, modified) values (?, ?, ?, ?, ?, ?, ?)",
        [uuid, bank.accountNumber, bank.accountType, bank.userId, bank.bankId, Date.now(), Date.now()], callback);
    }
};

module.exports = bankAccount;
