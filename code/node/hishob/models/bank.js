var db = require('../config/dbconnection');

var bank = {
    getBanks : function (callback) {
        return db.query("SELECT * FROM bank", callback);
    },
    getBankById : function (bankId, callback) {
            return db.query("SELECT * FROM bank WHERE bank_id = ?", [bankId], callback);
    },
    createBank : function (bank, uuid, callback) {
        return db.query("INSERT INTO bank (uuid, bank_name) values (?, ?)", [uuid, bank.bankName], callback) ;
    }
};

module.exports = bank;
