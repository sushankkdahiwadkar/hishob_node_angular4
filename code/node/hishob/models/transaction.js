var db = require('../config/dbconnection');

var transaction = {
    getTransactions : function (callback) {
        return db.query("SELECT * FROM transaction", callback);
    },
    getTransactionsByTransactionId : function (transactionId, callback) {
        return db.query("SELECT * FROM transaction WHERE transaction_id = ?", [transactionId], callback);
    },
    getTransactionsByUserId : function (userId, callback) {
        return db.query("SELECT * FROM transaction WHERE user_id = ?", [userId], callback);
    },
    createTransaction : function (transaction, uuid, callback) {
        return db.query("INSERT INTO transaction (uuid, user_id, description, amount, transaction_type, bank_account_id, cash_account_id, category, created, modified) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
        [uuid, transaction.userId, transaction.description, transaction.amount, transaction.transactiontype, transaction.bankAccountId, transaction.cashAccountId, transaction.category, Date.now(), Date.now()], callback);
    }
};

module.exports = transaction;
