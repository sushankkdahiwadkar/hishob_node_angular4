var db = require('../config/dbconnection');

var cashAccount = {
    getCashAccounts : function (callback) {
        return db.query("SELECT * FROM cash_account", callback);
    },
    getCashAccountByCashAccountId : function (cashAccountId, callback) {
        return db.query("SELECT * FROM cash_account WHERE cash_account_id = ?", [cashAccountId], callback);
    },
    createCashAccount : function (userUserId, uuid, callback) {
        return db.query("INSERT INTO cash_account (uuid, user_id, created, modified) values (?, ?, ?, ?)", [uuid, userUserId.userId, Date.now(), Date.now()], callback);
    }
};

module.exports = cashAccount;
