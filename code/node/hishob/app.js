var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors=require('cors');

var index = require('./routes/index');
var users = require('./routes/users');
var cashAccounts = require('./routes/cash_accounts');
var bankAccounts = require('./routes/bank_accounts');
var banks = require('./routes/banks');
var transactions = require('./routes/transactions');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use('/', index);
app.use('/user', users);
app.use('/cashAccount', cashAccounts);
app.use('/bankAccount', bankAccounts);
app.use('/bank', banks);
app.use('/transaction', transactions);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  if (err.status == 404 && !err.error) {
      err.error = "Resource not found.";
  } else if (err.status == 500 && !err.error) {
      err.error = "Internal Server Error.";
  }

  res.status(err.status || 500);
  res.json(err);
});

module.exports = app;
