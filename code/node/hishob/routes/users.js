var express = require('express');
var uuid = require('node-uuid');
var router = express.Router();
var user = require('../models/user');

/* GET users listing. */
router.get('/', function(req, res, next) {
    user.getAllUsers(function(error, result) {
        if (error) {
            next({ status : 500, error : "Internal Server Error." });
        } else {
            res.json(result);
        }
    });
});

/* GET user by userId. */
router.get('/:userId', function(req, res, next) {
    user.getUserById(req.params.userId, function(error, result) {
        if (error) {
            next({ status : 500, error : "Internal Server Error." });
        } else {
            if (result.length > 0) {
                res.json(result);
            } else {
                next({status : 404 , error : "User not found with id " + req.params.userId });
            }
        }
    });
});

/* Create new User */
router.post('/', function(req, res, next){

    if (!req.body.firstName || !req.body.email || !req.body.mobileNumber) {
        next({ status : 400, error : "Please check whether you have specified firstName, lastName or mobileNumber." });
    } else if (isNaN(req.body.mobileNumber)) {
        next({ status : 400, error : "Please check the mobile number you have provided. It seems it is not a valid number." });
    } else {
        var uid = uuid.v4();
        user.createUser(req.body, uid, function (error, result){
            var createdUser = req.body;
            if (error) {
                next({ status : 500, error : "Internal Server Error." });
            } else {
                createdUser.userId = result.insertId;
                createdUser.uuid = uid;
                res.json(createdUser);
            }
        });
    }
});

/* Edit User by userId */
router.put('/:userId', function(req, res, next){

    if (!req.body.firstName || !req.body.email || !req.body.mobileNumber) {
        next({ status : 400, error : "Please check whether you have specified firstName, lastName or mobileNumber." });
    } else if (isNaN(req.body.mobileNumber)) {
        next({ status : 400, error : "Please check the mobile number you have provided. It seems it is not a valid number." });
    } else {
        user.updateUser(req.params.userId, req.body, function(error, result){
            if (error) {
                 next({ status : 500, error : "Internal Server Error." });
            } else {
                if (result.affectedRows == 0) {
                    next({ status : 400, error : "Please check the userId you provided. It seems User is not available for provided userId." });
                } else {
                    user.getUserById(req.params.userId, function(error, result) {
                        if (error) {
                            next({ status : 500, error : "Internal Server Error." });
                        } else {
                            if (result.length > 0) {
                                res.json(result);
                            } else {
                                next({ status : 400, error : "Please check the userId you provided. It seems User is not available for provided userId." });
                            }
                        }
                    });
                }
            }
        });
    }
});

module.exports = router;
