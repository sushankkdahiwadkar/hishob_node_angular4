var express = require('express');
var uuid = require('node-uuid');
var router = express.Router();
var cashAccount = require('../models/cash_account');

/* GET users listing. */
router.get('/', function(req, res, next) {
    cashAccount.getCashAccounts(function(error, result) {
        if (error) {
            next({ status : 500, error : "Internal Server Error." });
        } else {
            res.json(result);
        }
    });
});

/* GET user by userId. */
router.get('/:cashAccountId', function(req, res, next) {
    cashAccount.getCashAccountByCashAccountId(req.params.cashAccountId, function (error, result) {
        if (error) {
            next({ status : 500, error : "Internal Server Error." });
        } else {
            if (result.length > 0) {
                res.json(result);
            } else {
                next({status : 404 , error : "Cash Account not found with id " + req.params.cashAccountId });
            }
        }
    });
});

/* Create new User */
router.post('/', function(req, res, next){
    var uid = uuid.v4();
    cashAccount.createCashAccount(req.body, uid, function(error, result) {
      if (error) {
          next({ status : 500, error : "Internal Server Error." });
      } else {
          var createdCashAccount = req.body;
          createdCashAccount.cashAccountId = result.insertId;
          createdCashAccount.uuid = uid;
          res.json(createdCashAccount);
      }
    });
});

/*/* Edit User by userId
router.put('/:userId', function(req, res, next){
    user.updateUser(req.params.userId, req.body, function(error, result){
        if (error) {
            res.json(error);
        } else {
            res.json(req.body);
        }
    });
});*/

module.exports = router;
