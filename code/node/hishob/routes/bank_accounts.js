var express = require('express');
var uuid = require('node-uuid');
var router = express.Router();
var bankAccount = require('../models/bank_account');

/* GET users listing. */
router.get('/', function(req, res, next) {
    bankAccount.getBankAccounts(function(error, result) {
        if (error) {
            next({ status : 500, error : "Internal Server Error." });
        } else {
            res.json(result);
        }
    });
});

/* GET user by userId. */
router.get('/:bankAccountId', function(req, res, next) {
    bankAccount.getBankAccountByBankAccountId(req.params.bankAccountId, function (error, result) {
        if (error) {
            next({ status : 500, error : "Internal Server Error." });
        } else {
          if (result.length > 0) {
              res.json(result);
          } else {
              next({status : 404 , error : "Bank Account not found with id " + req.params.bankAccountId });
          }
        }
    });
});

/* Create new User */
router.post('/', function(req, res, next){
    var uid = uuid.v4();
    if (!req.body.accountNumber || !req.body.userId || !req.body.bankId) {
        next({ status : 400, error : "Please check whether you have specified accountNumber, userId or bankId."});
    } else {
      bankAccount.createBankAccount(req.body, uid, function(error, result) {
          if (error) {
              next({ status : 500, error : "Internal Server Error." });
          } else {
              var createdBankAccount = req.body;
              createdBankAccount.bankAccountId = result.insertId;
              createdBankAccount.uuid = uid;
              res.json(createdBankAccount);
          }
      });
    }
});

/*/* Edit User by userId
router.put('/:userId', function(req, res, next){
    user.updateUser(req.params.userId, req.body, function(error, result){
        if (error) {
            res.json(error);
        } else {
            res.json(req.body);
        }
    });
});*/

module.exports = router;
