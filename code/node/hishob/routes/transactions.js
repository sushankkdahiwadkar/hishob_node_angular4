var express = require('express');
var uuid = require('node-uuid');
var router = express.Router();
var transaction = require('../models/transaction');

/* GET users listing. */
router.get('/', function(req, res, next) {
    transaction.getTransactions(function(error, result) {
        if (error) {
            next({ status : 500, error : "Internal Server Error."});
        } else {
            res.json(result);
        }
    });
});

/* GET user by userId. */
router.get('/:transactionId', function(req, res, next) {
    transaction.getTransactionsByTransactionId(req.params.transactionId, function(error, result) {
        if (error) {
            next({ status : 500, error : "Internal Server Error."});
        } else {
          if (result.length > 0) {
              res.json(result);
          } else {
              next({status : 404 , error : "Transaction not found with transactionId " + req.params.transactionId });
          }
        }
    });
});

/* GET user by userId. */
router.get('/user/:userId', function(req, res, next) {
    transaction.getTransactionsByUserId(req.params.userId, function(error, result) {
        if (error) {
            next({ status : 500, error : "Internal Server Error."});
        } else {
          if (result.length > 0) {
              res.json(result);
          } else {
              next({status : 404 , error : "Transactions not found for userId " + req.params.userId });
          }
        }
    });
});

/* Create new User */
router.post('/', function(req, res, next){
    var uid = uuid.v4();
    if (!req.body.description || !req.body.transactionType ) {
        next({ status : 400, error : "Please check if whether you have specified description or transactionType."});
    } else {
      transaction.createTransaction(req.body, uid, function (error, result){
        var createdTransaction = null;
          if (error) {
              next({ status : 500, error : "Internal Server Error."});
          } else {
              createdTransaction = req.body;
              createdTransaction.transactionId = result.insertId;
              createdTransaction.uuid = uid;
              res.json(createdTransaction);
          }
      });
    }
});

/*/* Edit User by userId
router.put('/:userId', function(req, res, next){
    user.updateUser(req.params.userId, req.body, function(error, result){
        if (error) {
            res.json(error);
        } else {
            res.json(req.body);
        }
    });
});
*/
module.exports = router;
