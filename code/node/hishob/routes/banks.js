var express = require('express');
var uuid = require('node-uuid');
var router = express.Router();
var bank = require('../models/bank');

/* GET users listing. */
router.get('/', function(req, res, next) {
    bank.getBanks(function(error, result) {
        if (error) {
            next( { status : 500, error : "Internal Server Error." } );
        } else {
            res.json(result);
        }
    });
});

/* GET user by userId. */
router.get('/:bankId', function(req, res, next) {
    bank.getBankById(req.params.bankId, function(error, result) {
      if (error) {
          next({ status : 500, error : "Internal Server Error." });
      } else {
          if (result.length > 0) {
              res.json(result);
          } else {
              next({status : 404 , error : "Bank not found with id " + req.params.bankId });
          }
      }
    });
});

/* Create new User */
router.post('/', function(req, res, next){
    var uid = uuid.v4();
    var createdBank = null;
    if (!req.body.bankName) {
        next({ status : 400, error : "Please check whether you have provided bankName." });
    } else {
        bank.createBank(req.body, uid, function(error, result) {
            if (error) {
                next({ status : 500, error : "Internal Server Error." });
            } else {
                createdBank = req.body;
                createdBank.bankId = result.insertId;
                createdBank.uuid = uid;
                res.json(createdBank);
            }
        });
    }
});

module.exports = router;
