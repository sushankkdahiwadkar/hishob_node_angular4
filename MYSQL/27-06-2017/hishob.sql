-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema hishob
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema hishob
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `hishob` DEFAULT CHARACTER SET utf8 ;
USE `hishob` ;

-- -----------------------------------------------------
-- Table `hishob`.`bank`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hishob`.`bank` ;

CREATE TABLE IF NOT EXISTS `hishob`.`bank` (
  `uuid` VARCHAR(40) NULL DEFAULT NULL,
  `bank_id` INT(11) NOT NULL AUTO_INCREMENT,
  `bank_name` VARCHAR(50) NULL DEFAULT NULL,
  `created` VARCHAR(20) NULL DEFAULT NULL,
  `modified` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`bank_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 26
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hishob`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hishob`.`user` ;

CREATE TABLE IF NOT EXISTS `hishob`.`user` (
  `uuid` VARCHAR(40) NULL DEFAULT NULL,
  `user_id` BIGINT(8) NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(50) NOT NULL,
  `last_name` VARCHAR(50) NULL DEFAULT NULL,
  `middle_name` VARCHAR(50) NULL DEFAULT NULL,
  `user_name` VARCHAR(50) NULL DEFAULT NULL,
  `email` VARCHAR(50) NOT NULL,
  `mobile_number` BIGINT(8) NULL DEFAULT NULL,
  `passsword` VARCHAR(40) NULL DEFAULT NULL,
  `created` VARCHAR(20) NULL DEFAULT NULL,
  `modified` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hishob`.`bank_account`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hishob`.`bank_account` ;

CREATE TABLE IF NOT EXISTS `hishob`.`bank_account` (
  `uuid` VARCHAR(40) NULL DEFAULT NULL,
  `bank_account_id` BIGINT(8) NOT NULL AUTO_INCREMENT,
  `account_number` VARCHAR(50) NULL DEFAULT NULL,
  `account_type` VARCHAR(50) NULL DEFAULT NULL,
  `user_id` BIGINT(8) NOT NULL,
  `bank_id` INT(11) NULL DEFAULT NULL,
  `created` VARCHAR(20) NULL DEFAULT NULL,
  `modified` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`bank_account_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hishob`.`cash_account`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hishob`.`cash_account` ;

CREATE TABLE IF NOT EXISTS `hishob`.`cash_account` (
  `uuid` VARCHAR(40) NULL DEFAULT NULL,
  `cash_account_id` BIGINT(8) NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT(8) NULL DEFAULT NULL,
  `created` VARCHAR(20) NULL DEFAULT NULL,
  `modified` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`cash_account_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hishob`.`transaction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hishob`.`transaction` ;

CREATE TABLE IF NOT EXISTS `hishob`.`transaction` (
  `uuid` VARCHAR(40) NULL DEFAULT NULL,
  `transaction_id` BIGINT(8) NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT(8) NULL DEFAULT NULL,
  `description` VARCHAR(100) NULL DEFAULT NULL,
  `amount` INT(11) NULL DEFAULT NULL,
  `transaction_type` VARCHAR(6) NULL DEFAULT NULL,
  `bank_account_id` BIGINT(8) NULL DEFAULT NULL,
  `cash_account_id` BIGINT(8) NULL DEFAULT NULL,
  `category` VARCHAR(20) NULL DEFAULT NULL,
  `created` VARCHAR(20) NULL DEFAULT NULL,
  `modified` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`transaction_id`),
  INDEX `bank_account_id_idx` (`bank_account_id` ASC),
  INDEX `cash_account_id_idx` (`cash_account_id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
